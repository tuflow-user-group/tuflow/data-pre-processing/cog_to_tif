from osgeo import gdal

import glob
import os

def tif_to_tif(path):
    files = glob.glob(path)
    for file in files:
        filename = os.path.splitext(file)[0]
        output = f'{filename}_converted.tif'
        gdal.Translate(output, file, options="-of GTiff -ot float32 -co BIGTIFF=NO -co COMPRESS=deflate -co Predictor=2 -co Zlevel=9 ")
        print("COG GeoTIFF files have been converted to a TIF format supported by TUFLOW")

tif_to_tif(r"ADD PATH WHERE COG GEOTIFFS ARE LOCATED\*.tif")